
linux="$(cat /home/fer/Descargas/listado.txt | grep "Linux" | awk '{print $3}')"

windows="$(cat /home/fer/Descargas/listado.txt | grep "Windows" | awk '{print $3}')"

totalLinuxUsers=0
totalLinuxProces=0

totalWindowsUsers=0
totalWindowsProces=0

for i in $linux; do
	totalLinuxUsers="$(expr $totalLinuxUsers + 1 )"
	totalLinuxProces="$(expr $totalLinuxProces + $i )"
	#echo "$i"
done

for i in $windows; do
	totalWindowsUsers="$(expr $totalWindowsUsers + 1 )"
	totalWindowsProces="$(expr $totalWindowsProces + $i )"
	#echo "$i"
done

echo "Linux -> $totalLinuxUsers $totalLinuxProces"
echo "Windows -> $totalWindowsUsers $totalWindowsProces"

