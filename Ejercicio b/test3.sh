litros="$(cat /home/fer/Descargas/precipitaciones.txt | awk '{ print $2}')"
dias="$(cat /home/fer/Descargas/precipitaciones.txt | awk '{ print $1}')"
index=0
dia=0

echo "Dias que no llovieron: "

for i in $litros; do 
	index="$(expr $index + 1 )"
	dia="$(expr $dia + 1 )"
	if [ $index -gt 7 ]; then
		index=1
	fi
	if [ $i -eq 0 ]; then 
		if [ $index -eq 1 ]; then
			echo "Lunes $dia"
		elif [ $index -eq 2 ]; then
			echo "Martes $dia"
		elif [ $index -eq 3 ]; then
			echo "Miercoles $dia"
		elif [ $index -eq 4 ]; then
			echo "Jueves $dia"
		elif [ $index -eq 5 ]; then
			echo "Viernes $dia"
		elif [ $index -eq 6 ]; then
			echo "Sabado $dia"
		elif [ $index -eq 7 ]; then
			echo "Domingo $dia"
		fi
	fi					
done


