echo "Introduce la cantidad de litros de agua consumidos"

read i
precioTramo1=20
precioTramo2=30
calculo1=0
calculo2=0
calculo3=0

temp=$( echo "($i+0.5)/1" | bc)

if [ $i -lt 51 ]; then
	calculo1=$( echo "scale=2; $i * 20" | bc ) 
	calculo1=$( echo "scale=2; $calculo1 / 50" | bc)
	echo "Precio final $calculo1 €"
elif [ $temp -lt 201 ]; then
	restantes=$( echo "scale=5; $i - 50" | bc )
	echo "Litros despues del tramo1 $restantes"	
	calculo2=$( echo "scale=5; $restantes * 0.2" | bc )
	echo "Total despues del tramo1 $calculo2 €"
	calculo2=$( echo "scale=5; $calculo2 + $precioTramo1" | bc )
	echo "Precio final $calculo2 €"
else		
	echo "Total despues del tramo1 $precioTramo1 €"
	echo "Total despues del tramo2 $precioTramo2 €"

	restantes=$( echo "scale=5; $i - 200" | bc )
	echo "Litros despues del tramo2 $restantes"
	calculo3=$( echo "scale=5; $restantes * 0.1" | bc )
	echo "Total despues del tramo2 $calculo3 €"
	calculo3=$( echo "scale=5; $calculo3 + $precioTramo1" + $precioTramo2 | bc )
	echo "Precio final $calculo3 €"
fi
	

