echo "Ingrese una nota"

read i

if [ $i ]; then
	temp="$(echo "($i+0.5)/1" | bc)"			
	
	if [ $temp -lt 0 ]; then
		echo "Error valor menor de 0"	
		. /home/fer/Descargas/test2.sh
	else		
		if [ $temp -lt 5 ]; then
			echo "Suspendido"
		elif [ $temp -lt 6 ]; then
			echo "Aprobado"
		elif [ $temp -lt 7 ]; then
			echo "Bien"
		elif [ $temp -lt 9 ]; then
			echo "Notable"
		elif [ $temp -lt 11 ]; then
			echo "SobreSaliente"
		fi		
	fi
else
	echo "Error valor nulo"
	. /home/fer/Descargas/test2.sh
fi


